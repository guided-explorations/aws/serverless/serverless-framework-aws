"use strict";

const APP_URL = process.env.APP_URL;
const GL_ENV = process.env.GL_ENV;

const html = `
<!DOCTYPE html>
  <html>
  <head>
      <title>GitLab Serverless Framework example</title>
  </head>
  <body BGCOLOR="white">
      <H2>Environment: ${GL_ENV}</H2>
      <p>Click on the button to run your function. You can send a parameter too!</p>
      <label>
      Param value:
      <input type="text" id="param" placeholder="Input your param value" name="yourParam">
      </label>
      <br>
      <button>Run function</button>
      <p>Function Output:</p>
      <p id="functionOutput"></p>

      <script>

      const functionUrl = "${APP_URL}"

      document.querySelector('button').addEventListener('click', () => {
        const paramValue = document.querySelector('#param').value;
      
        fetch(functionUrl, {
          method: 'POST',
          mode: "cors", 
          body: JSON.stringify({myParam: paramValue}), 
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .then(response => response.json())
        .then(json => {
          document.querySelector('#functionOutput').textContent = JSON.stringify(json)
        });  
      });
      </script>
  </body>
  </html>`;

module.exports.webui = async function(event) {
 
    // check for GET params and use if available
    //if (event.queryStringParameters && event.queryStringParameters.name) {
    //  dynamicHtml = `<p>Hey ${event.queryStringParameters.name}!</p>`;
    //}

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/html"
      },
      body: html,
    };
  };
  