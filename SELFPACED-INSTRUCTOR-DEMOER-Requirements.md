## Prerequisite Resources for All Tutorials

| Item                                                         | How To Create It                                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| A GitLab User ID on GitLab.com                               | [GitLab.com Sign Up Page](https://gitlab.com/users/sign_up)<br />(email confirmation required) |
| An AWS Account                                               | [Create Account Page](https://aws.amazon.com/resources/create-account/)<br />(Credit Card Required but not charged to enable Free Trial Resources)<br />These tutorials use only use free tier resources.<br />**For instructor-led courses**, the instructor may provide this. |
| A GitLab.com Namespace Organizational Tenant with Ultimate license (New group at root of GitLab.com) | Once logged into GitLab.com, simply create a new unique group name as a root group, such as https://gitlab.com/mygroup555. In the group, go to “Billing” and enable a 30 day Ultimate trial.<br />It is important to have access to Ultimate features - so only choose a self-managed instance if your organization is licensed for GitLab Ultimate.<br />**For instructor-led courses**, the instructor may provide this. |
| A Computer<br />**Note:** no changes will need to be done to the laptop and the operating system does not matter as the Web IDE is how all code editing occurs in these workshops. | All project changes are done in the Web IDE so that no local configuration of editors or runtimes is required - which keeps classroom environments moving along. |
| Access to Public Projects in https://gitlab.com/guided-explorations/aws/ (or exports of these projects if air gapped) | Only a concern if you are attempting to use a private GitLab Instance. Using a new organizational tenant on GitLab.com allows enablement of a 30 day ultimate trial. |
| A GitLab Self-Managed Runner (Easy Button Setup Provided)    | Only a concern if you are attempting to use a private GitLab Instance. Using a new organizational tenant on GitLab.com gives free runner minutes with credit card activation.<br />**For instructor-led courses**, the instructor may provide this. |



## Tutorial 1 - Serverless Framework Deployment to AWS with GitLab Serverless SAST Scanning

| Exercises | Self-Paced Participant<br />(Default Assumption) | Classroom Instructor                                | Classroom Participant   | Demoer      |
| --------- | ------------------------------------------------ | --------------------------------------------------- | ----------------------- | ----------- |
| 1.1       | **Perform**                                      | Prestage                                            | **Perform**             | Prestage    |
| 1.2 - 1.3 | **Perform**                                      | Prestage                                            | Use Instructor Prestage | Prestage    |
| 1.4       | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed | **Perform**             | Prestage    |
| 1.5-End   | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed | **Perform**             | **Perform** |

## Tutorial 2 - Secure Serverless Framework Development with GitLab Security Policy Approval Rules and Managed DevOps Environments

| Exercises | Self-Paced Participant<br />(Default Assumption) | Classroom Instructor                                         | Classroom Participant   | Demoer      |
| --------- | ------------------------------------------------ | ------------------------------------------------------------ | ----------------------- | ----------- |
| 1.1-End   | Completed                                        | Completed                                                    | Completed               | Completed   |
| 2.1       | **Perform**                                      | Can Prestage for Class Group Level                           | Use Instructor Prestage | Prestage    |
| 2.2-2.5   | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed<br />If you are using a shared AWS account for all students, perform or 2.2, Steps 11-18 and publish the KMS arn to the class. Preferable in a location where they can copy it directly rather than type it. | **Perform**             | Prestage    |
| 2.6-End   | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed          | **Perform**             | **Perform** |

## Tutorial 3.1 - DAST API Scanning

| Exercises        | Self-Paced Participant<br />(Default Assumption) | Classroom Instrutor                                 | Classroom Participant | Demoer      |
| ---------------- | ------------------------------------------------ | --------------------------------------------------- | --------------------- | ----------- |
| 1.1-End, 2.1-End | Completed                                        | Completed                                           | Completed             | Completed   |
| 3.1.1-?          | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed | **Perform**           | Prestage    |
| 3.2.1=?          | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed | **Perform**           | **Perform** |

## Tutorial 3.2 - Serverless Framework to AWS with Long Lived Shared Stage Branch Environment

| Exercises        | Self-Paced Participant<br />(Default Assumption) | Classroom Instrutor                                 | Classroom Participant | Demoer      |
| ---------------- | ------------------------------------------------ | --------------------------------------------------- | --------------------- | ----------- |
| 1.1-End, 2.1-End | Completed                                        | Completed                                           | Completed             | Completed   |
| Tutorial 3.1 | Not Required | Not Required | Not Required | Not Required |
| 3.2.1-3.2.2          | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed | **Perform**           | Prestage    |
| 3.2.3-End          | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed | **Perform**           | **Perform** |

## Tutorial 3.3 - Amazon CodeGuru Secure AI Scanning with Security Policy Approval Rules

| Exercises        | Self-Paced Participant<br />(Default Assumption) | Classroom Instrutor                                 | Classroom Participant | Demoer      |
| ---------------- | ------------------------------------------------ | --------------------------------------------------- | --------------------- | ----------- |
| 1.1-End, 2.1-End | Completed                                        | Completed                                           | Completed             | Completed   |
| Tutorial 3.2       | Not Required                           | Not Required                              | Not Required | Not Required |
| 3.3.2             | **Perform**                                      | Prestage | Use Instructor Prestage*           | Prestage    |
| 3.3.2-End                | **Perform**                                      | Validation Pre-Run<br />In Class: Perform as Needed | **Perform**           | **Perform** |

