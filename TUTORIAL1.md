# Tutorial 1: Serverless Framework Deployment to AWS with GitLab Serverless SAST Scanning

## Known Working Version Details

Tested Date: **2023-10-27**

Testing Version (GitLab and Runner): **Enterprise Edition 16.3.0 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

## Visual Overview

![Tutorial 1 Visual Overview](images/serverlesstutorial1overview.png)

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- The labs do not direct you to watch the creation and destruction of the Lambda functions, applications, API gateway, etc - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.
- ![](images/gitlab-logo-20.png) **[Game Changer]** = GitLab Value That is Game Changing. These are of special interest if you are using this tutorial as a Rapid Proof of Concept (POC) of GitLab’s capabilities.
- ![](images/gitlab-logo-20.png) = Unique GitLab Value.

> ![](images/gitlab-logo-20.png) **[Game Changer]** Callouts like these will be used to note unique learning points about the steps just completed. 

## Concepts To Watch For

- Many CI systems use ‘build failure’ on shared branches as the primary or exclusive way to force software defects to be dealt with. 
  - There are a number of downsides to this approach if other controls are possible - especially with security vulnerability defects. For instance, having to wait on assistance before any more work can be done. Also, the pain of one-by-one-failures discovery of vulnerabilities.
  - GitLab has very flexible approvals - some of which only become required under the right conditions. Since GitLabs Merge Request process supports this for security vulnerabilities, it prevents a the very disruptive workflow of failing pipelines on vulnerabilities, but still does not allow them to be merged into the next branch. This maximizes developer productivity.
- GitLab’s empowered shift left maximizes the ‘in-context’ help for developers to handle vulnerabilities and it is focused on the code changes in the developers Merge Request / Feature branch so that there is clear responsibility for the introduction of defects right as they happen.
- The SAST scanning results are Serverless Framework specific because the Kics IaC SAST scanner has implemented findings and security best practices for Serverless Framework.
- Notice that GitLab’s built-in Infrastructure as Code tooling (based on Kics) finds many Serverless specific security issues.

## Exercises

### 1.1 Creating GitLab Group and Project

1. Start by creating a new project in the GitLab group of your choosing on the GitLab instance of your choosing
   Throughout these exercises this group will be represented as `full/path/to/yourgroup`.

2. Use the GitLab UI to **navigate to** ‘full/path/to/yourgroup’

3. On the left navigation, **Click** ‘Group overview’

4. Near the bottom right of the page, **Click** ‘Create new project’ (Large panel)

5. On the *Create new project* page, **Click** ‘Import project’ (Large panel)

6. On the *Import project* page, **Click** ‘Repository by URL’ (upper right button)

   > Note: Page expands into the Repository by URL form.

7. Find field *Git repository URL*, **Type or Paste** https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws.git
   Throughout these exercises this project will be represented as `full/path/to/yourgroup/serverless-framework-aws`

8. **Click** ‘Create project’ (bottom left button)

   > Note: The Project overview page will display for your new project.

### 1.2 Creating AWS IAM User and Deployment Permissions

> The Serverless Framework helps managed least privileges for the actual running of your application. These permissions are for deployment.

1. Open a browser tab and **login** to the AWS account of your choosing.

2. You should be at the URL https://console.aws.amazon.com 

3. In the Search box **Type** ‘IAM’ and *in the results list*, **Click** “IAM”

4. From the *Identity and Access Management (IAM*) page, on the left nav bar, **Click** ‘Users’

5. **Click** ‘Create user’ (upper right button)

6. On *Specify user details*, **Type or Paste** ‘gitlab-serverless-deploy-user’ and **Click** ‘Next’

7. On *Set Permissions*, **Click** ‘Next’

8. On *Review and create*, **Click** ‘Create user’

9. On *Users*, under *User name*, **Click** ‘gitlab-serverless-deploy-user’ 

10. On *Add permissions* button, **Click** {the expand down arrow} and **Click** ‘Create inline policy’

11. On *Specify permissions*, **Click** ‘JSON’

12. In *Policy editor*, **Delete** {all contents}

13. **Paste** {the contents of https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws/-/blob/prod/deploythisserverlessapp.iam.json}

14. **Click** ‘Next’

15. On *Review and create*, in ‘Policy name’, **Type or Paste** ‘deploythisserverlessapp’ and **Click** ‘Create policy’

    > Note: You will be sent back to the user details screen

16. While here in *gitlab-serverless-deploy-user*, **Click** ‘Security credentials’ (mid-page tab)

17. To the right of *Access keys*, **Click** ‘Create access key’

18. On *Access key best practices & alternatives*, **Click** ‘Command Line Interface (CLI)’

19. Near the page bottom, **Select** ‘I understand the above recommendation and want to proceed to create an access key.’

20. **Click** ‘Next’

21. On *Set description tag - optional*, **Click** ‘Create access key’

22. **Note:** Next to *Access key* and *Secret access key* there are copy icons (looks like a two document stack)

23. Leave this tab open so that the copy icons can be used in the next steps.

### 1.3 Configuring GitLab CI Variables for AWS Keys

1. Open a browser tab to `full/path/to/yourgroup/serverless-framework-aws`

2. On the left navigation, **Click** ‘Settings => CI/CD’

3. To the right of *Variables*, **Click** ‘Expand’

4. Use **Add variable** once for each table row and specify the variables settings as indicated in the table. 

   You will use the browser tab left open to the AWS Create Keys page earlier.

   If the AWS Create Keys tab was closed, it will be necessary to create a new set of AWS Keys

   | Key                   | Value                                                        | Protect<br />(IMPORTANT!) | Mask |
   | --------------------- | ------------------------------------------------------------ | ------------------------- | ---- |
   | AWS_ACCESS_KEY_ID     | Copy from “Access key” in IAM Keys page left open earlier    | No                        | No   |
   | AWS_SECRET_ACCESS_KEY | Copy from “Secret access key” in IAM Keys page left open earlier | No                        | Yes  |
   | AWS_REGION            | us-east-1 (or another region you have access to)             | No                        | No   |

### 1.4 Use Merge Request Single Source of Truth To See Security Findings

In this section you will create a GitLab Issue and Merge Request. It is important to work on code using a Merge Request so that we can see all defects associate with just the code we’ve changed on our branch - including security vulnerabilities - which are also ONLY for code we’ve changed or added.

`[ONLY FOR LEARNING]`Since we’ve never run a pipeline on a the default branch, all existing security vulnerabilities will appear in the MR as if we just created them all in our branch. This works well for ensuring training exercises are shorter because presaged vulnerabilities show up in MRs, but in the real world, all vulnerabilities already on the default branch would not be shown - only vulnerabilities that we introduced with our MR code changes. The fact that MR Vulnerability Findings are from our code changes dramatically shifts the responsibility and desire to eliminate them like all other code defects.

1. While in a browser tab on `full/path/to/yourgroup/serverless-framework-aws`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘Updates’

4. **Click** ‘Create issue’

5. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. **Click** ‘Create merge request’

   > **Merge Requests - Developer’s Single Source of Truth and Collaboration**
   > GitLab’s work flow from Issue to Merge Request automatically creates a branch to work on. By initiating this workflow from the issue, there is immediate visibility that work has started and all the artifacts are cleanly linked together. The branch and merge request are already associated and the issue will be closed upon successfully merging. The Merge Request is also the Single Source of Truth (SSOT) for all changes and change approvals for the developer and all collaborators and approvers.
   >
   > When enabled, Merge Requests are also capable of creating an isolated DevOps environment for the code changes - which enables all kinds of CI checks that require a running version of the application, including: DAST Scanning, API Fuzzing, Accessibility Testing, Performance Testing, Browser Testing and any Human QA that is not yet automated.

8. On *Resolve “Updates” {new merge request view}*

9. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

   **Note**: A new tab opens with VS Code editing a copy of your project.

   > GitLab supports multiple hosted code editing environments. The WebIDE is a VS Code based editor for all your files. GitPod enables developer environments along with the IDE environment. Single file editing is also available for quick changes.

10. On the left navigation, **Right Click** ‘TUTORIAL.gitlab-ci.yml’

11. Near the bottom of the popup menu, **Click** ‘Rename...’

12. Remove the the string `A_TUTORIAL` so that the file name is only `.gitlab-ci.yml` (keep the leading ".")

13. If you are performing these tutorials in an instructor led classroom, find out if the instructor has asked you to edit the top of the .gitlab-ci.yml file to look something like this (the actual value between the single quotes would be different)
    ```yaml
    default:
      tags: [ 'a-special-string-the-instructor-gives-you' ]
    ```

14. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

15. In *Commit message*, **Type** ‘Add scanning’

16. **Click** ‘Commit to ’1-updates’‘

    > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

17. **Close** the Web IDE tab.

18. **Click** {the Merge Request tab from which the IDE was launched}

19. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

20. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

21. **Click** {the icon under ‘Status’} (probably reads either ‘running’, ‘failed’ or ‘passed

    > Note: The Pipeline view opens.

22. **Click** {the browser back button}
    If you cannot click the browser back button, recenter your location by browsing to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

23. Wait For all jobs to complete successfully (Pipeline Status will be ‘Passed’).

    > If all jobs on the most recent pipeline do not complete successfully, there may have been a conflict between two pipelines, if this happens, **Click** ‘Run pipeline’ (a button)

    > Optional: If you have access to the AWS console and deployed to us-east-1, you can use this link to see your new Lambda Application: https://us-east-1.console.aws.amazon.com/lambda/home?region=us-east-1#/applications

24. On the left navigation, **Click** ‘Secure => Security configuration => Vulnerability Management’ (last click is a tab under the page heading)

25. On *Security configuration*, under *Security training*, **Click** each of the {slider buttons next to ‘Kontra’, ‘Secure Code Warrior’, ‘SecureFlag’}

26. On the left navigation, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

    > `[ONLY FOR LEARNING]` As mentioned before, Merge Requests generally only contain the results of CI checks for code that has been changed in the Merge Request itself. So all defects and vulnerabilities are for code the developer has just changed. In this case there are presaged vulnerabilities and since our very first pipeline is for a new merge request, it looks as if we created the entire code base in our Merge Request and therefore serves to show all kinds of findings from GitLab scanning. In real world setups the developer is very focused on the findings because they pertain only to changes they just made.

27. Near the top, under the MR title, **Click** ‘Overview’ ( a tab)

28. There should be expandable sections for “License Compliance” and “Security scanning detected…” - if they are not visible or not yet able to be expanded, keep refreshing your browser until the sections are completely populated and expandable.

### 1.5 Examining Finds in MR

1. Use this screenshot to explore the security and SBOM MR Widget content:

   ![image-20230801130233601](images/mr11.png)

   > ![](images/gitlab-logo-20.png) **[Game Changer]** Within the development daily habit loop, only for code the developer changed, they are immediately notified of vulnerabilities that were inadvertently added. 

2. Click on of the findings titled ‘Improper limitation of a pathname to a restricted directory (’Path Traversal’)’ and use this screenshot to explore: 

   ![image-20230801084020947](images/security1.png)

   > ![](images/gitlab-logo-20.png) **[Game Changer]** The full context of a vulnerability as well as collaboration (Dismiss/Create Issue) and training capabilities (Training: section) are immediately available for vulnerabilities that were just added to the codebase. 

### 1.6 Testing Deployed API

> The Pages form is just for convenience so that we do not have to use a utility to post to the application URL.

1. On the left navigation (while browsing  `full/path/to/yourgroup/serverless-framework-aws`)

2. **Click** ‘Operate => Environments’

3. In the environments list, to the right of *1-updates-{number}*, **Click** ‘Open’ (button)

   > Note that if we had 5 developers each working on a feature branch, there would be 5 independent environments and if we had deployed at least once to production, it’s environment would display as well.

4. Notice the page heading **Environment: 1-updates-{number}** tells us which environment we are in.

5. On the new tab, for *Param value*, **Type** ‘Testing’

6. **Click** ‘Run function’

7. Under *Function Output* {review the function’s json return value}

   > You just used a serverless function displaying a webform to talk to an api gateway that ran a second serverless function to send back the results that are displayed.

8. Close the tab with the webform you just used.

### Security Policy Approval Rules and Managed DevOps Environments Tutorial

Explore how GitLab Security Policy rules can require special merge approvals for a wide variety of vulnerability criteria by moving on to: [Secure Serverless Framework Development with Security Policy Approval Rules and Managed DevOps Environments](TUTORIAL2-SecurityAndManagedEnvs.md)