# GitLab Enterprise DevOps Blueprint: Serverless App on AWS Lambda

This working example employs the [Serverless Framework](https://serverless.com) to deploy a simple application to AWS Lambda.

[Authored as an Open Educational Resource (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

# Just Want a Working Example Without Tutorials?

After making a copy of this repository and adding the CI variables: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY and AWS_REGION, simply rename `A_READYTOGO.gitlab-ci.yml` to `A_READYTOGO.gitlab-ci.yml`

# GitLab Developer Experiences and Game Changers

The following are some of the valuable things that this working code and the tutorials demonstrate.

![](images/gitlab-logo-20.png) = Unique GitLab Value.
![](images/gitlab-logo-20.png) **[Game Changer]** = GitLab Value That is Game Changing.

| **Valuable Developer Experiences and Solutions Demonstrated** | [Tutorial 1](TUTORIAL1.md) | [Tutorial 2](TUTORIAL2-SecurityAndManagedEnvs.md) | [Tutorial 3.1](TUTORIAL3.1-DASTScanning.md) | [Tutorial 3.2](TUTORIAL3.2-Environments.md) | [Tutorial 3.3](TUTORIAL3.3-AmazonCodeGuruWithSecurityPolicies.md) |
| :----------------------------------------------------------- | :------------------------: | :-----------------------------------------------: | :-----------------------------------------: | :-----------------------------------------: | :----------------------------------------------------------: |
| **Time Required** (Hands On Mins / Automation Wait Mins)     |           15m/5m           |                      15m/10m                      |                   15m/10m                   |                   10m/5m                    |                           20m/10m                            |
| **Reusable Pattern for Serverless Framework Deployment to AWS**<br />Integrating GitLab’s environment management functionality with Serverless Framework’s built-in environments capability. |     :white_check_mark:     |                :white_check_mark:                 |             :white_check_mark:              |             :white_check_mark:              |                      :white_check_mark:                      |
| ![](images/gitlab-logo-20.png) **[Game Changer] GitLab Merge Request-First Workflow**<br /> **Game Changer Because**: Agile’s ‘Transparency’ value is fully enabled as work is immediately visible, an MR is a single source of truth for per-feature code changes, CI results and approvals, Value Stream Metrics can be accurate and there is less room for human errors in trying to map issues => merge requests => code changes. |     :white_check_mark:     |                :white_check_mark:                 |             :white_check_mark:              |             :white_check_mark:              |                      :white_check_mark:                      |
| ![](images/gitlab-logo-20.png) **Serverless Specific Security Findings**<br />GitLab’s Kics IaC Scanner scans for Serverless Framework specific findings and best practices. Additional IaC scanners can be employed. |     :white_check_mark:     |                :white_check_mark:                 |             :white_check_mark:              |                                             |                      :white_check_mark:                      |
| ![](images/gitlab-logo-20.png) **[Game Changer] Environments as Code for Lifecycle Managed DevOps Environments**<br />**Game Changer Because**: Every branch in this project uses GitOps to Automatically (no human intervention) deploy an Isolated, DevOps (on production like infra) Lifecycle Managed (provision through destroy) Environment. This capability is used to deliver many of the other features in this table. Also a Game Changer in that it demonstrates leveraging maximum value from the concept of GitOps. |     :white_check_mark:     |                :white_check_mark:                 |             :white_check_mark:              |             :white_check_mark:              |                      :white_check_mark:                      |
| ![](images/gitlab-logo-20.png) **[Game Changer] Merge Request (MR) Lifecycle Managed DevOps Environments**<br />**Game Changer Because**: automatic provisioning and disposal of Environments that are alive and continuously updated for as long as the MR is open allows DAST and all other Dynamic testing while the feature is being developed. This means all dynamic testing is shifted as far left as is possible. |     :white_check_mark:     |                :white_check_mark:                 |             :white_check_mark:              |                                             |                      :white_check_mark:                      |
| ![](images/gitlab-logo-20.png) **[Game Changer] MR Findings Only for Changed Code**<br />**Game Changer Because**: Developers know that new findings are only for code they recently changed in the current MR. The DRI for the code changes is clearly known to everyone. |                            |                :white_check_mark:                 |             :white_check_mark:              |                                             |                      :white_check_mark:                      |
| ![](images/gitlab-logo-20.png) **[Game Changer] Policy Enforced Security MR Approvals**<br />**Game Changer Because**: developer productivity is not impacted by the bad legacy approach of using CI failures to control vulnerabilities progressing to production. Developer can simply eliminate the offending vulnerabilities they accidentally added to avoid seeking security approval. Configurable for any scanner. |                            |                :white_check_mark:                 |             :white_check_mark:              |             :white_check_mark:              |                      :white_check_mark:                      |
| ![](images/gitlab-logo-20.png) **[Game Changer] DAST Scanning Shifted Left To Feature Development**<br />**Game Changer Because**: There is no process delay nor lack of clear responsibility compared to the general approach of delaying DAST scanning to a shared branch. This is enabled by the “Lifecycle Managed DevOps Environments” Game Changer mentioned above. |                            |                                                   |             :white_check_mark:              |                                             |                                                              |
| ![](images/gitlab-logo-20.png) **Pre Production Long-lived Lifecycle Environments**<br />Long Lived GitOps Managed DevOps Environments. (e.g. feature => dev => staging => prod). Merging to branches to accomplish environment promotion. Full MR approvals and controls applied to deployment. This is enabled by the “Lifecycle Managed DevOps Environments” Game Changer mentioned above. This is enabled by the “Lifecycle Managed DevOps Environments” Game Changer mentioned above. |                            |                                                   |                                             |             :white_check_mark:              |                                                              |
| ![](images/gitlab-logo-20.png) **On Demand Lifecycle Managed DevOps Environments**<br />Self-service, on-demand environments for experiments - including older releases. This is enabled by the “Lifecycle Managed DevOps Environments” Game Changer mentioned above. |                            |                                                   |                                             |             :white_check_mark:              |                                                              |
| ![](images/gitlab-logo-20.png) **[Game Changer] Amazon CodeGuru Secure AI Scanning Integration with GitLab Secure**<br />Full integration of results in Merge Requests, Security Dashboards and Security Policy Merge Approvals. **Game Changer Because**: any scanner whose results can be coerced into GitLab’s SAST json format can create first class scanning results throughout the GitLab product. |                            |                                                   |                                             |                                             |                      :white_check_mark:                      |

## Implementation Details

**Why does every environment and cloud resource include the GitLab project ID?**

Read about it in the comments for variable TARGET_ENV in [.gitlab-ci.yml](.gitlab-ci.yml) - you can disable it if you can guarantee stack uniqueness by another method - such as deployment of only one GitLab project per AWS account.

**Sometimes I don't want an environment, just all the other CI to run. How can I do that?**

The default configuration will not create environments for any branch that contains the string 'no-env'. This string can be customize in [.gitlab-ci.yml](.gitlab-ci.yml)
by modifying the variable NO_ENV_BRANCH_PATTERN which holds a GitLab CI YML Regex pattern that governs what the string(s) is/are for suppressing environments.

## Special Branch Names

For the sake of advanced deployment capabilities, GitLab treats environments with the following names as special - enabling Group level protected environments. Since the simplest and most GitOps compatible way to map environments is to map your repository branches to environment names (as is done in this project), you should also consider using only branch names that match these patterns in order to keep CI and GitLab controls configuration as simple as possible and aligned with GitLab’s built-in environment tiers.

Branch name equates to the environment name in this project by convention - this relationship is not hard coded into GitLab, so should not be assumed of other projects or templates.

List of advised branch names (which become environment names in this example):

| Branch name (Environment name) pattern - case insensitive comparison | Environment Tier |
| ------------------------------------------------------------ | ---------------- |
| \*prd\*, \*prod\*, \*live\*                                  | Production       |
| \*stg\*, \*stag*, \*modl\*, \*model\*, \*pre\*, \*demo\*, \*non\* | Staging          |
| \*test\*, \*tst\*, \*int\*, \*ac\*, \*acpt\*, \*accept\*, \*qa\*, \*qc\*, \*control\*, \*quality\* | Test             |
| \*dev\*,\*review\*, \*trunk\*                                | Development      |
| * (anything not matched above)                               | Other            |

Documentation: [Deployment tier of environments](https://docs.gitlab.com/ee/ci/environments/index.html#deployment-tier-of-environments), [keyword environment:deployment_tier](https://docs.gitlab.com/ee/ci/yaml/index.html#environmentdeployment_tier)

# Tutorials That Use This Working Code

**IMPORTANT:** Requirements for self-paced execution of these tutorials are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)

If you are an instructor or using this for demos, please consult [INSTRUCTOR-DEMOER-Doc.md](INSTRUCTOR-DEMOER-Doc.md)

[TUTORIAL1.md](TUTORIAL1.md) - Covers the basics of deploying a serverless application to a private merge request isolated Managed DevOps Environment and performing Serverless Framework SAST on the application

[TUTORIAL2-SecurityAndManagedEnvs.md](TUTORIAL2-SecurityAndManagedEnvs.md) - builds upon TUTORIAL1.md to demonstrate Security Policy Merge Approval Rules and how they encourage a developer to resolve new vulnerabilities in the code changes they’ve made before being allowed to merge into a shared branch. It also shows how Managed DevOps Environments allow full preview of application changes in an Per-Merge Request isolated environment and how that environment is automatically destroyed when merging into the default branch to update the Production environment.

[TUTORIAL3.1-DASTScanning.md](TUTORIAL3.1-DASTScanning.md) - shows DAST scanning with security policy merge approvals and differentials findings in action. Shifting DAST scanning all the way left into feature development is what caused GitLab to build MR Lifecycle Managed DevOps Environments.

[TUTORIAL3.2-Environments.md](TUTORIAL3.2-Environments.md) - shows how the branch based environments that make MR environments possible also open up a world of possibilities for GitOps workflows through multiple pre-production environments (e.g. Feature => Integration => Stage => Production). Also shows how branches can function as self-service experimental environment provisioning and disposal - including for older versions of the stack.

[TUTORIAL3.3-AmazonCodeGuruWithSecurityPolicies.md](TUTORIAL3.3-AmazonCodeGuruWithSecurityPolicies.md) - shows how GitLab can fully incorporate the 3rd party security scanner “Amazon CodeGuru Secure” - including ingesting findings into GitLab Merge Requests, Security Dashboards and being able to block merges using Security Policy Merge Approval rules.


# Visual Overview

## Tutorial 1

![Tutorial 1 Visual Overview](images/serverlesstutorial1overview.png)

## Tutorial 2

![Tutorial 2 Visual Overview](images/serverlesstutorial2overview.png)

## Tutorial 2

![Tutorial 2 Visual Overview](images/serverlesstutorial2overview.png)

## Tutorial 3

![serverlesstutorial3gitlabworkflow](images/serverlesstutorial3gitlabworkflow.png)

![](images/gitlabbranchlifecycledevopsenvironments.png)

# Example

GitLabs Standard Security Scanning does a great job of Serverless Specific Security Scanning.
![](images/mr11.png)