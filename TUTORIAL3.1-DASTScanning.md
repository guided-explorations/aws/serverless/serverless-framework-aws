# Tutorial 3.1 - DAST API Scanning

## Known Working Version Details

Tested Date: **2024-04-19**

Testing Version (GitLab and Runner): **Enterprise Edition 16.11 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** 

- General Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
- Tutorials 1 and 2 must also be completed and the resultant project available to start this tutorial.

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

## Visual Overview

![Tutorial 1 Visual Overview](images/serverlesstutorial1overview.png)

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- The labs do not direct you to watch the AWS console activities triggered by GitLab - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.
- ![](images/gitlab-logo-20.png) **[Game Changer]** = GitLab Value That is Game Changing. These are of special interest if you are using this tutorial as a Rapid Proof of Concept (POC) of GitLab’s capabilities.
- ![](images/gitlab-logo-20.png) = Unique GitLab Value.

> ![](images/gitlab-logo-20.png) **[Game Changer]** Callouts like these will be used to note unique learning points about the steps just completed. 

## Concepts To Watch For

- ![](images/gitlab-logo-20.png) **[Game Changer] Security Findings Only For Changed Code**
- ![](images/gitlab-logo-20.png) **[Game Changer] DAST Security Policy Merge Approvals in Feature Branches**
- ![](images/gitlab-logo-20.png) **[Game Changer] Frictionless Security Policy** 

## Exercises

### 3.1.1 Update Security Policy Merge Approval To Block on DAST Findings

1. [Tutorial 1](TUTORIAL1.md) and [Tutorial 2](TUTORIAL2-SecurityAndManagedEnvs.md) must be completed first so that the project is in the correct state for the following to work. Throughout these exercises this project will be represented as `full/path/to/yourgroup/serverless-framework-aws`

2. While in a browser tab on `full/path/to/yourgroup/serverless-framework-aws`

3. **Click** ‘Plan => Issues’

4. On the upper right, **Click** ‘New issue’ (button)

5. On *New Issue*, for *Title (required)*, **Type** ‘New Color’

6. **Click** ‘Create issue’

7. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

8. On *New merge request*, **Uncheck** ‘Mark as draft’

9. Leave all other settings at their defaults

10. At the bottom of the page, **Click** ‘Create merge request’

11. On *Resolve “New Color” {new merge request view}*

12. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

    > **Note**: A new tab opens with VS Code editing a copy of your project.

13. On the left navigation, **Click** ‘frontend’ (folder) and then ‘handler.js’

14. **Locate** ‘<body bgcolor="white">’

15. Change `white` to `lightgreen`

16. It should look like this:

    ```html
    <body bgcolor="lightgreen"> 
    ```

    > You have heard that support has a hard time understanding what version a customer is running when debugging problems, so you will add the version number to the function output to make this easy.

17. On {the left side file navigation}, **Click** ‘src’ and then **Click** ‘handler.js’ (not under “frontend” folder)

18. Locate:

    ````javascript
    message: 'Your function executed successfully!',
    ````

19. Add `Version: 1.3.7` right after the exclamation mark, the final line should look like this:

    ```javascript
    message: 'Your function executed successfully! Version: 1.3.7',
    ```

20. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

21. In *Commit message*, **Type** ‘Change color’

22. **Click** ‘Commit to ’2-new-color’

23. Close the current Web IDE tab and switch back to the last browser tab where the Merge Request view is.

24. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “New Color” ’ 

25. Watch the pipeline status at the top of the Overview section. It starts with “Merge request pipeline #111111111 running”

    > Once again the security reports are not loading and all Security Policy Merge Approvals are required until the pipeline processing our changes finishes.

26. Wait For all jobs in the new pipeline to complete successfully (Pipeline Status will be ‘Passed’).

    > If the pipeline does not seem to be progressing, use the left navigation menu “Build => Pipelines” to view the status.

27. Refresh the page.

28. Notice that the Merge Request is now blocked by a Merge Approval (1) & (2) that was previously optional becoming required. This was due to the security policy merge approval rules the project is configured for.

29. The GitLab Security Bot message is also emailed to the developer.(5)

    > ![](images/gitlab-logo-20.png) **[Game Changer] Security Findings Only For Changed Code:** There were quite a few findings in the initial code that we merged to the default branch and they now appear in the Vulnerability report Security Dashboard - but no longer appear here (3). The findings in the MR, however, only list newly detect licenses and vulnerabilities in the code changes of this specific merge request. FYI: The secrets are re-detected because we have a special setting on to always surface those findings regardless of what commit they are a part of.

    > ![](images/gitlab-logo-20.png) **[Game Changer] DAST Security Policy Merge Approvals in Feature Branches:** The merge is blocked because it violates a DAST policy because the developer added what seemed to be an innocuous change. Shifting DAST scanning left into feature development requires the GitLab MR Lifecycle DevOps Environment (![](gitlab-logo-20.png) **[Game Changer]**) so that continuous DAST testing can be done in the development daily habit loop.

    ![image-20240422145211493](images/secondmr2.png)

30. In the *Merge Request*, on the section heading title starting with *Security scanning detected…*, **Click** {the down expansion arrow}

31. There should be only one finding, **Click** the blue title text that start with *Medium Version Number Exposed via ‘POST’…* (4)

32. The finding in the screenshot below shows why the version number is a problem (1) and shows that the function output is sending the version information (2).

    ![findingdetail](images/findingdetail.png)

    > ![](images/gitlab-logo-20.png) **[Game Changer] Semi Private Remediation Opportunity:** At this point the developer would only need to seek security approval if they want to justify with the Security Approvers why this vulnerability needs to be allowed into production. If they simply eliminate the accidental vulnerability, the approval rule will again be optional. 
    >
    > 
    >
    > ![](images/gitlab-logo-20.png) **[Game Changer] Vulnerability Management that Honors Developer Productivity** Unlike failing builds on new security findings, Security Policy Merge Approvals allow a developer to keep working on their solution code while waiting for help with actions on vulnerabilities. If they need to wait for a collaborator or even an approval process for having the vulnerability excepted, they can keep working on their feature without risk to the company that the vulnerability will be ignored. The developer can keep being productive while the company keeps being safe.

33. We will now eliminate the vulnerability to see that the developer will be allowed to merge without any oversight involvement from anyone.

34. Near the top right, find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

   > **Note**: A new tab opens with VS Code editing a copy of your project.

34. On the left navigation, **Click** ‘frontend’ (folder) and then ‘handler.js’

12. On {the left side file navigation}, **Click** ‘src’ and then **Click** ‘handler.js’ (not under “frontend” folder)

13. Locate:

    ````javascript
    message: 'Your function executed successfully! Version: 1.3.7',
    ````

14. And remove `Version: 1.3.7` right after the exclamation mark, the final line should look like this:

    ```javascript
    message: 'Your function executed successfully!',
    ```

15. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

16. Click** ‘Commit to ’2-new-color’

17. Close the current Web IDE tab and switch back to the last browser tab where the Merge Request view is.

18. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/serverless-framework-aws` and **Click** ‘Code => Merge requests => Resolve “New Color” ’ 

19. Watch the pipeline status at the top of the Overview section. It starts with “Merge request pipeline #111111111 running”

    > Once again the security reports are not loading and all Security Policy Merge Approvals are required until the pipeline processing our changes finishes.

20. Wait For all jobs in the new pipeline to complete successfully (Pipeline Status will be ‘Passed’).

    > If the pipeline does not seem to be progressing, use the left navigation menu “Build => Pipelines” to view the status.

21. Refresh the page.

22. Now approval is optional (1),(2) and the MR is ready to merge (3),(4). The GitLab Security Bot has notified the developer (5)
    

    > ![image-20240422153112466](images/mresolved.png)

    > ![](images/gitlab-logo-20.png) **[Game Changer] Frictionless Security Policy:** The developer was able to self-resolve a vulnerability that they had inadvertently added. It was obvious to them that they had added it and they were made aware at a time where they are still changing the code - rather than days or weeks after shipping the code.
