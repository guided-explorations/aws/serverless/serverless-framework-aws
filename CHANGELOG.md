# Changelog

All notable changes to this project will be documented in this file.

## [v1.1] - 2024-04-22

- ADDED DAST API Scanning
- ADDED [Game Changer] Notifications where appropriate