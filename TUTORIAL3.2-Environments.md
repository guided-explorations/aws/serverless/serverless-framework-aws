# TUTORIAL 3.2: Serverless Framework to AWS with Long Lived Shared stage Branch Environment

## Known Working Version Details

Tested Date: **2023-08-21**

Testing Version (GitLab and Runner): **Enterprise Edition 16.4.0 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/serverless/serverless-framework-aws/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** 

- General Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
- Tutorials 1 and 2 must also be completed and the resultant project available to start this tutorial.

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

## Visual Overview

![serverlesstutorial3gitlabworkflow](images/serverlesstutorial3gitlabworkflow.png)

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- The labs do not direct you to watch the AWS console activities triggered by GitLab - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.
- ![](images/gitlab-logo-20.png) **[Game Changer]** = GitLab Value That is Game Changing. These are of special interest if you are using this tutorial as a Rapid Proof of Concept (POC) of GitLab’s capabilities.
- ![](images/gitlab-logo-20.png) = Unique GitLab Value.

> ![](images/gitlab-logo-20.png) **[Game Changer]** Callouts like these will be used to note unique learning points about the steps just completed. 

## Concepts To Watch For

- Simply by adding a new, long lived branch called ‘stage’ and protecting it, we can alter the Git Workflow to enable a long lived pre-production environment for integrating multiple branches into a production release.

## Exercises

### 3.2.1 Creating and Configuring a Long Lived stage Branch

1. [Tutorial 1](TUTORIAL1.md) and [Tutorial 2](TUTORIAL2-SecurityAndManagedEnvs.md) must be completed first so that the project is in the correct state for the following to work. Throughout these exercises this project will be represented as `full/path/to/yourgroup/serverless-framework-aws`

2. While browsing `full/path/to/yourgroup/serverless-framework-aws`, on the left navigation **Click** ‘Code => Branches ” ’ 

4. In the upper right **Click** ‘New branch’

5. In *Branch name*, **Type** ‘stage’ and **Click** ‘Create branch’

6. On the left navigation, **Click** ‘Settings => Repository’ (**NOT** ‘Code => Repository’)

7. Next to *Branch defaults*, **Click** ‘Expand’

8. Under *Default branch*, **Select** ‘stage’

9. At the bottom of the expanded Branch defaults section, **Click** ‘Save changes’

   > The section will collapse.

10. Next to *Protected branches*, **Click** ‘Expand’

11. Near the upper right of the expanded section, **Click** ‘Add protected branch’

12. In the *Protect a branch* form, for *Branch*, **Select** ‘stage’

13. In *Allowed to merge*, **Select** ‘Developers + Maintainers’

14. In *Allowed to push and merge*, **Select** ‘No one’

15. **Click** ‘Protect’

16. In the list of protected branches, for *Prod*, under *Allowed to push and merge*, **Select** ‘No one’

### 3.2.2 Observing the New Long Lived Environment

1. While browsing `full/path/to/yourgroup/serverless-framework-aws`, on the left navigation **Click** ‘Build => Pipelines ’

2. All pipelines should have completed successfully, if they have not, wait for completion (or resolve problems if any have occurred).

3. Once all pipelines have completed successfully, on the right navigation, **Click** ‘Operate => Environments’

   > There should be two environments, one starting with “prod-” and one starting with “stage-”

4. On each environment line, **Click** ‘Open’ to see the environments are separate.

   > Optional: If you have access to the AWS console and deployed to us-east-1, you can use these links to see stack resources for both environments. The new ones should start with `prod`: https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#Instances, https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#SecurityGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#AutoScalingGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#LoadBalancers
   
   > ![](images/gitlab-logo-20.png) **[Game Changer] Environments as Code** leverage true GitOps to create an environment for every branch in this project. This opens up a world of possibilities for which environments can be used.
   >
   > The usefulness of this capability is discussed below.

### 3.2.3 Implied New Git Workflow

With stage identified as a the default branch and protected, the following Git Workflow would now be typical. This is not a lab exercise - simply read the numbered steps to understand the Git workflow.

1. Create new merge requests from new issues, 

   > This will create new feature branches from ‘stage’ (from branch will now be ‘stage’ because it was made the default branch) 
   > => **This results in a new MR Lifecycle Branch and Environmen**t.

2. Merge multiple completed feature branches back to ‘stage’ 

   > The default merge target branch will now be ‘stage’ because it was made the default branch. Merge conflicts between parallel work are resolved at this time. These MRs default to “Delete” the source branch which is critical for MR Environment environment cleanup.

3. When ready to release the candidate release that has been accumulating in the stage branch, Create a merge request that targets ‘prod’

   > Since ‘stage’ is protected, the branch “Delete” option will not be available - which is good because it is meant to be long lived. This merge request is purely for GitOps environment deployment. Maintainers and higher scan still stop the environment from the “Operate => Environments” menu which might be needed to completely reset a bad environment.

4. Create a Git tag and GitLab Release for the merged commit in the prod branch.

As shown in this visual we’ve just moved prod to be a deployment only branch and created a stage branch for direct feature merge.

> ![](images/gitlab-logo-20.png) **[Game Changer] Environments for Internal Developer Platforms are Simple** using GitLab Environments as Code. The API for creating branches and destroying environments and deleting branches can be automated via an existing Developer Platform system.

![serverlesstutorial3gitlabworkflow](images/serverlesstutorial3gitlabworkflow.png)

### Environments as Code: Workflow or Experimental Environments As Needed

> ![](images/gitlab-logo-20.png) **[Game Changer] Environments as Code** leverage true GitOps to create an environment for every branch in this project. This opens up a world of possibilities for which environments can be used.

Some notable environments as code uses:

1. The ability for DAST and other dynamic testing to be shifted left into feature development depends on GitLab’s Merge Request Lifecycle Environments which are provided by Environments as Code
2. Some organizations may insert additional long lived branches and environments in their standard workflow.
3. An additional need is experimental environments that will never be merged. Sometimes an environment needs to be created to test upgrade scenarios or an older customer environment. In this project you can experiment with creating a branch called “experiment1” and pretend you are going to make some direct changes the cloud operating environment. Once you have your information, you can easily dispose of the environment and delete the branch.
4. Some organizations create, use and destroy complete environments during the processing of a pipeline - while these are not a part of this, it is easily coddle.
5. Notice one of the experiments is checking out an old version (v1.2) of the code to setup a previous instance of the environment and application to experiment on something. This would be creating a branch from a historical production release tag.

![serverlesstutorial3gitlabenvironments](images/gitlabbranchlifecycledevopsenvironments.png)

### 3.2.4 Optional: Create an “experiment1” Environment

1. While browsing `full/path/to/yourgroup/serverless-framework-aws`, on the left navigation **Click** ‘Code => Branches ” ’ 

2. For *Branch name*, **Type** ‘experiment1’ and **Click** ‘Create branch’

3. On the left navigation **Click** ‘Code => Branches ” ’ 

4. In the upper right **Click** ‘New branch’

5. In *Branch name*, **Type** ‘experiment1’ and **Click** ‘Create branch’

   > Notice the branch source defaults to ‘stage’ since the last project set it to be the default.

6. When the pipeline for ‘experiment1’ is done running, use “Operate => Environments” to visit the environment homepage.

   > Since this is not a long lived environment, we do not protect the branch.

7. To remove the branch, **Click** ‘Operate => Environments’

8. To the right of *experiment-{projectid}*, **Click** ‘Stop’

9. **Click** ‘Build => Jobs’

10. Wait for the job ‘destroy_env’ to complete.

11. To remove the branch, **Click** ‘Code => Branches’

12. To the far right of the branch name, click the three dots and **Select** ‘Delete branch’

> Protected environments (and any other environment) can be stopped by Maintainers and higher from the “Operate => Environments” page

> ![](images/gitlab-logo-20.png) **[Game Changer] Environments as Code** when GitOps is combined with Environments as Code, any number of experimental environments are easy to create and destroy.
