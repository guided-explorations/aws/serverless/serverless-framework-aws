
The content in this repository is offered as [Open Educational Resources](https://creativecommons.org/about/education-oer/)

This approach includes:
1. Formatting in plain markdown in an open source repository to enhance accessibility rather than being encoded in a proprietary format or a restricted system.
2. [Creative Commons Licensing](LICENSE.md) (attribution required) for tutorials and all other educational content.
3. Authored according to Hyperscaling Enablement Content Architecture (HECA) created by [Darwin Sanoy](@DarwinJS). This is mostly evident in [INSTRUCTOR-DEMOER-Doc.md](INSTRUCTOR-DEMOER-Doc.md) where guidance is given for Self-Paced Learning, Classroom Delivery and Demo Delivery. It also serves to: Bring instructors / demoers up to speed as well as be reusable by those individuals to conduct classroom training.